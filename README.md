# awx-job-init

Python script for start job in AWX.


# Options
```
  -h, --help                     Show this help message and exit
  -t TOKEN, --token=TOKEN        Token for AWX authentication.
  -d DESTINATION, --destination=DESTINATION
                                 AWX's DNS e.g. awx.example.local
  -i ID, --template-id=ID        ID of the used template.
  -e VARS, --extra-vars=VARS     Extra vars for job template

```

# Examples

```
# Install requirements
pip install -r requirements.txt

# Start script
src/main.py -t XBpnPmIDkFfYkf83m2ET3eK8g123mk -d awx.example.local -i 17 -e "key: value"
```