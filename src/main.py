#! /usr/local/bin/python3

import time
import awx

awx_token, awx_destination, awx_template_id, awx_extra_vars = awx.opt_vars_from_cli()

try:
    awx.ping(awx_destination, awx_token)
    print('Hostname and token are valid')
except Exception:
    print(f"Something goes wrong on ping stage\nCheck host {awx_destination} and token {awx_token}")
    exit(3)


js_body = ""
if awx_extra_vars is not None:
    js_body = awx.js_body_job_init(awx_extra_vars)

try:
    job_id = awx.job_init(awx_destination, awx_token, awx_template_id, js_body)
    print(f"Job in queue, id = {job_id}")
except Exception:
    print(f"Something goes wrong on job_init stage\nCheck template id {awx_template_id}")
    exit(2)

print(f"Link to logs:\nhttp://{awx_destination}/#/jobs/playbook/{job_id}/output")

job_status = ''
while job_status != 'successful' and job_status != 'failed':
    time.sleep(10)
    job_status, job_started, job_elapsed = awx.job_status(awx_destination, awx_token, job_id)
    if job_status == 'pending':
        print("Job still in queue")
    elif job_status == 'running':
        print("Job running")

exit_m = f"Job started at {job_started}\nJob was in progress {job_elapsed} seconds\nJob status {job_status}"

if job_status == 'failed':
    print(exit_m)
    exit(1)

print(exit_m)
