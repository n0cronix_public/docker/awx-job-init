import urllib3
import json
from datetime import datetime
from optparse import OptionParser


def opt_vars_from_cli():
    parg = OptionParser()
    parg.add_option("-t", "--token", type="string", dest="awx_token", help="Token for AWX authentication.",
                    metavar="TOKEN")
    parg.add_option("-d", "--destination", type="string", dest="awx_destination", help="AWX's destination e.g. awx.example.local",
                    metavar="DESTINATION")
    parg.add_option("-i", "--template-id", type="int", dest="awx_template_id", help="ID of the used template.",
                    metavar="ID")
    parg.add_option("-e", "--extra-vars", type="string", dest="awx_extra_vars", help="Extra vars for job template",
                    metavar="VARS", action="append")
    options = parg.parse_args()[0]
    return options.awx_token, options.awx_destination, options.awx_template_id, options.awx_extra_vars


def ping(host, token):
    http = urllib3.PoolManager()
    url = 'http://' + str(host) + '/api/v2/ping/'
    head = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token}
    json.loads(http.request('GET', url, headers=head).data.decode('utf-8'))


def js_body_job_init(extra_vars):
    new_dict = dict()
    for i in range(len(extra_vars)):
        j = extra_vars[i].strip().split(sep=":")
        new_dict.update({j[0].strip(): j[1].strip()})
    final_dict = dict()
    final_dict.update({"extra_vars": new_dict})
    js_body = json.dumps(final_dict)
    return js_body


def job_init(host, token, template_id, js_body):
    http = urllib3.PoolManager()
    url = 'http://' + str(host) + '/api/v2/job_templates/' + str(template_id) + '/launch/'
    head = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token}
    job_id = json.loads(http.request('POST', url, headers=head, body=js_body).data.decode('utf-8'))['id']
    return job_id


def job_status(host, token, job_id):
    http = urllib3.PoolManager()
    url = 'http://' + str(host) + '/api/v2/jobs/' + str(job_id) + '/'
    head = {'Content-Type': 'application/json', 'Authorization': 'Bearer ' + token}
    rq = http.request('GET', url, headers=head)
    js_data = json.loads(rq.data.decode('utf-8'))
    status = js_data['status']
    elapsed = js_data['elapsed']
    started_isoformat = js_data['started']
    started_time = ''
    if started_isoformat is not None:
        started = datetime.strptime(started_isoformat, "%Y-%m-%dT%H:%M:%S.%f%z")
        started_time = str(started.hour) + ':' + str(started.minute) + ':' + str(started.second)
    return status, started_time, elapsed
