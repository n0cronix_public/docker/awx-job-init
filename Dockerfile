FROM python:3.9.5-slim
WORKDIR /opt
COPY . .
RUN pip install --no-cache-dir -r requirements.txt
ENTRYPOINT ["/opt/src/main.py", "$@"]